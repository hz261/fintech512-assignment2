
var ready = (callback) => {
    if (document.readyState != "loading") callback();
    else document.addEventListener("DOMContentLoaded", callback);
  }
  
  ready(() => { 
      /* Do things after DOM has fully loaded */ 
      makeplot1();
  });
///
var selectInput = document.getElementById('selectInput');
var button = document.getElementById('button');

selectInput.addEventListener('change', function() {
    var selectedValue = selectInput.value;
    var data;
    if (selectedValue === 'data1') {
        makeplot1();
    } else if (selectedValue === 'data2') {
        makeplot2();
    } else {
        makeplot3();
    }
  });
////////////////////
  function makeplot1() {
    console.log("makeplot: start")
    fetch("includes/stock_data/AAPL.csv")
    .then((response) => response.text()) /* asynchronous */
    .catch(error => {
        alert(error)
         })
    .then((text) => {
      console.log("csv: start")
      csv().fromString(text).then(processData)  /* asynchronous */
      console.log("csv: end")
    })
    console.log("makeplot: end")
  
  };

  function makeplot2() {
    console.log("makeplot: start")
    fetch("includes/stock_data/GME.csv")
    .then((response) => response.text()) /* asynchronous */
    .catch(error => {
        alert(error)
         })
    .then((text) => {
      console.log("csv: start")
      csv().fromString(text).then(processData)  /* asynchronous */
      console.log("csv: end")
    })
    console.log("makeplot: end")
  
  };

  function makeplot3() {
    console.log("makeplot: start")
    fetch("includes/stock_data/SPY.csv")
    .then((response) => response.text()) /* asynchronous */
    .catch(error => {
        alert(error)
         })
    .then((text) => {
      console.log("csv: start")
      csv().fromString(text).then(processData)  /* asynchronous */
      console.log("csv: end")
    })
    console.log("makeplot: end")
  
  };
  ////////////////////////////
  
  function processData(data) {
  console.log("processData: start")
  let x = [], y = []
  
  for (let i=0; i<data.length; i++) {
      row = data[i];
      x.push( row['Date'] );
      y.push( row['Close'] );
  } 
  makePlotly( x, y );
  console.log("processData: end")
  }
  
  function makePlotly( x, y ){
  console.log("makePlotly: start")
  var traces = [{
        x: x,
        y: y
  }];
  var layout  = { title: "Apple Stock Price History"}
  
  myDiv = document.getElementById('plotDiv');
  Plotly.newPlot( myDiv, traces, layout );
  console.log("makePlotly: end")
  };

  button.addEventListener('click', function() {
    Plotly.update('plotDiv', data1, layout);
  });
  ///button///
  const submitButton = document.getElementById("submitButton");
  
  submitButton.addEventListener("click", function() {
    Swal.fire({
      title: 'Are you sure?',
      text: "You want to submit the page?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Form Submitted!',
          '',
          'success'
        )
      } else {
        Swal.fire(
          'Action Cancelled',
          '',
          'error'
        )
      }
    })
  });
  